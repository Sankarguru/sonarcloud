package com.ey.de;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EySpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(EySpringApplication.class, args);
	}

}
