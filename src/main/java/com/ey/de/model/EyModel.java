package com.ey.de.model;

public class EyModel {
	
	public EyModel(String firstName) {
		super();
		this.firstName = firstName;
	}
	public EyModel(String firstName, String lastName, String city, String country, int userId) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.city = city;
		this.country = country;
		this.userId = userId;
	}
	private String firstName;
	private String lastName;
	private String city;
	private String country;
	private int userId;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	
}
