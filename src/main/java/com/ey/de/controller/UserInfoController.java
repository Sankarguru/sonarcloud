package com.ey.de.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ey.de.model.EyModel;

@RestController
public class UserInfoController {


		private static final String TEMPLATE = "Hello, %s!";

		@GetMapping("/userInfo")
		public EyModel greeting(@RequestParam(value = "name", defaultValue = "Sankarguru") String name) {
			return new EyModel(String.format(TEMPLATE, name));
		}
	
}
