package com.ey.de;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ey.de.model.EyModel;

public class EyModelTest {
	
    public static final String EXPECTED_FIRST_NAME = "Sankarguru";
    public static final String EXPECTED_LAST_NAME = "Thirugnanasambandam";
    public static final int EXPECTED_USERID = 101;
    private EyModel eyModel;
    @Before
    public void setUp()  {
    	System.out.println("Start Test");
    	eyModel = new EyModel("Sankarguru", "Thirugnanasambandam", "Dublin","Ireland",101);
    }

 
	@Test
    public void testUserDetails() {
        Assert.assertEquals(EXPECTED_FIRST_NAME, eyModel.getFirstName());
        Assert.assertEquals(EXPECTED_LAST_NAME, eyModel.getLastName());
        Assert.assertEquals(EXPECTED_USERID, eyModel.getUserId());

    }
	
}
